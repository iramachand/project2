#Project 2 
#ASTR 344
#Raodkill Vultures


from matplotlib import pyplot as plt
from matplotlib import animation as a
import numpy as np
import matplotlib.cm as cm
plt.ion()


class Car():
	
	def __init__(self, x=0, dx=1, bumper = None):
		self.x = x
		self.bumper = bumper
		self.dx = dx

	def move(self):
		self.x = self.x + self.dx
	
	def change_dx(self, dx):
		self.dx = dx
		
	def seeDeer(self, slow):
		#probability
		#acceleration change by fraction (slow)
		#if np.random.randint(0,10) > 1 :
		self.dx = np.float32(slow * self.dx)
        #       else:
         #               self.x = self.x + self.dx
		

#New additions here
#in python, can do linked list functionality with lists, so no need to do linked list
CarNumber = 5
car_list = []
car_pos = []
graph_y = []
for i in range(0,CarNumber):
	if i == 0:
		new_car = Car(25, 5, None)
		car_list.append(new_car)
	else:
		new_car = Car(car_list[i-1].x - 5, car_list[i-1].dx, car_list[i-1])
		car_list.append(new_car)
	car_pos.append(new_car.x)
	graph_y.append(20)

while car_list[len(car_list) -1].x < 100 :
	print ("pos of last car: ", car_list[len(car_list) -1].x)
	del car_pos[:]
	del graph_y[:]
	carselect = 3
	for i in range(0,CarNumber):
		if car_list[len(car_list) -1].x == 50:
			if i == carselect:
				car_list[i].seeDeer(.25)
				#print carselect
		if i>carselect:
			car_list[i].change_dx(car_list[i-1].dx)
		car_list[i].move()
		#print (car_list[i].dx)
		car_pos.append(car_list[i].x)
		graph_y.append(20)

	colours = cm.rainbow(np.linspace(0,1,len(graph_y)))
	plt.scatter(car_pos,graph_y,color = colours)
	plt.axis([0, 100, 10, 30])
	plt.draw()
	plt.pause(.1)
	plt.clf()
	plt.title("Highway to Deer") 
	
t = 0
time = []
carselect = np.random.randint(0,(CarNumber - 1))
print "The",carselect,"th car has seen a deer."
car_list[carselect].seeDeer(.5)
while car_list[-1].x < 100:
	for n in range(len(car_list) - carselect):
		if 0.<=car_list[n].x<=100.:
			car_list[carselect+n].dx = car_list[carselect].dx
	for n in range(len(car_list)):
		car_list[n].move()
	t = t + 1
	time.append(t)
print "The time of completion is",t,"time steps."
